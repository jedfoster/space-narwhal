'use strict'

require 'angular'
require 'angular-ui-router'
# require './index'
# require './gist'

usePreloadedOrFetchTemplate = (template) ->
  ($http, $stateParams) ->
    preloaded = document.querySelector('[data-preloaded]')

    if preloaded
      return ''
    else
      console.log('fetching...')
      $http.get(template)
        .then (data) ->
          data.data


angular.module('AirtexApp', [
  'ui.router'
  # 'SassMeister.gist'
  # 'SassMeister.index'
])

.config ($stateProvider, $urlRouterProvider, $locationProvider) ->
  $locationProvider.html5Mode true

  $urlRouterProvider.otherwise '/'

  $stateProvider
    .state('application',
      abstract: true
      url: '/'
      templateProvider: usePreloadedOrFetchTemplate('/_application.html')
      controller: 'ApplicationController'
    )
    .state('application.index',
      url: ''
      templateProvider: usePreloadedOrFetchTemplate('/_index.html')
      controller: 'ApplicationController'
    )
    .state('application.about-us',
      url: 'about-us'
      templateProvider: usePreloadedOrFetchTemplate('/_about-us.html')
      controller: 'ApplicationController'
    )
    .state('application.commercial-products',
      url: 'commercial-products'
      templateProvider: usePreloadedOrFetchTemplate('/_commercial-products.html')
      controller: 'ApplicationController'
    )
    .state('application.retail-products',
      url: 'retail-products'
      templateProvider: usePreloadedOrFetchTemplate('/_retail-products.html')
      controller: 'ApplicationController'
    )
    .state('application.contact-us',
      url: 'contact-us'
      templateProvider: usePreloadedOrFetchTemplate('/_contact-us.html')
      controller: 'ApplicationController'
    )
    .state('application.shopping-cart',
      url: 'shopping-cart'
      templateProvider: usePreloadedOrFetchTemplate('/_shopping-cart.html')
      controller: 'ApplicationController'
    )
  
.run [
  '$rootScope'
  '$state'
  '$stateParams'
  ($rootScope, $state, $stateParams) ->
    $rootScope.$on('$viewContentLoaded', (event) ->
      preloaded = document.querySelector('[data-preloaded]')
      preloaded.removeAttribute('data-preloaded') if preloaded
    )
  
    $rootScope.$state = $state
    $rootScope.$stateParams = $stateParams
]

.controller 'ApplicationController', ($scope) ->
  # no-op

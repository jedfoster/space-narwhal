var dest = './build';
var src = './src';

module.exports = {
  sass: {
    src: src + '/css/*.{sass,scss}',
    dest: dest + '/css/',
    settings: {
      indentedSyntax: true,
      imagePath: 'images',
      includePaths: ['./bower_components']
    }
  },
  images: {
    src: src + "/img/**",
    dest: dest + "/img"
  },
  markup: {
    src: [src + '/*.jade'],
    dest: dest
  },
  browserify: {
    bundleConfigs: [{
      entries: src + '/js/app.coffee',
      dest: dest + '/js/',
      outputName: 'app.js',
      extensions: ['.coffee', '.hbs']
    }]
  },
  production: {
    cssSrc: dest + '/css/*.css',
    jsSrc: dest + '/js/*.js',
    dest: dest
  }
};

